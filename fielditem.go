package apidoc

import "reflect"

type FieldItem struct {
	name    string
	clazz   string
	remark  string
	extdata string
}

func isObject(clazz string) bool {
	switch clazz {
	case "int":
		return false
	case "uint":
		return false
	case "bool":
		return false
	case "int8":
		return false
	case "int16":
		return false
	case "int32":
		return false
	case "int64":
		return false
	case "uint8":
		return false
	case "uint16":
		return false
	case "uint32":
		return false
	case "uint64":
		return false
	case "string":
		return false
	case "float32":
		return false
	case "float64":
		return false
	default:
		return true
	}
}

func (self *FieldItem) init(field reflect.StructField) {
	self.clazz = field.Type.Name()

	extdata := field.Tag.Get("required")
	if extdata == "true" {
		self.extdata = "required"
	} else {
		self.extdata = "optional"
	}

	name, ok := field.Tag.Lookup("form")
	if ok {
		self.name = name
	} else {
		name, ok = field.Tag.Lookup("form")
		if ok {
			self.name = name
		} else {
			self.name = field.Name
		}
	}

	name, ok = field.Tag.Lookup("name")
	if ok {
		self.remark = name
	} else {
		name, ok = field.Tag.Lookup("remark")
		if ok {
			self.remark = name
		}
	}
}
