package apidoc

import (
	"fmt"
	"sort"
	"strconv"
	"strings"
	"github.com/gin-gonic/gin"
)

func param(ctx *gin.Context, name string) string {
	val := ctx.Query(name)
	if val == "" {
		return ctx.Request.PostFormValue(name)
	} else {
		return val
	}
}

func cgidoc(ctx *gin.Context) {
	padding := param(ctx, "padding")
	path := param(ctx, "path")

	doc := GetDocument(path)
	rspdoc := doc.Response
	reqdoc := doc.Request
	remark := doc.Remark
	temp := getTemplate()

	if reqdoc == "" && rspdoc == "" && remark == "" {
		reqdoc = "<td style='color:#B00;height:24px'>没有参数或没有编写文档</td><td></td><td></td><td></td>"
		rspdoc = reqdoc
	}

	if padding == "" {
		padding = "2vh 20vw"
	}

	reqdoc = replace(reqdoc, "<td>required</td>", "<td>必填</td>")
	rspdoc = replace(rspdoc, "<td>required</td>", "<td>必填</td>")
	reqdoc = replace(reqdoc, "<td>optional</td>", "<td>可选</td>")
	rspdoc = replace(rspdoc, "<td>optional</td>", "<td>可选</td>")

	path = replace(path, "/", ".")
	temp = replace(temp, "<%=path%>", path)
	temp = replace(temp, "<%=remark%>", remark)
	temp = replace(temp, "<%=reqdoc%>", reqdoc)
	temp = replace(temp, "<%=rspdoc%>", rspdoc)
	temp = replace(temp, "<%=padding%>", padding)

	ctx.Header("Content-Type", "text/html;charset=utf-8")
	ctx.String(200, temp)
}

func getcgilist(ctx *gin.Context) {
	type Item struct {
		Path    string `json:"path"`
		Remark  string `json:"remark"`
		Extdata string `json:"extdata"`
		Enabled int    `json:"enabled"`
	}

	type Response struct {
		Code int    `json:"code"`
		List []Item `json:"list"`
	}

	num := 0
	flag := -1
	auth := -1
	path := param(ctx, "path")
	access := param(ctx, "access")
	enabled := param(ctx, "enabled")

	if len(access) > 0 {
		val, err := strconv.Atoi(access)
		if err == nil {
			auth = val
		}
	}

	if len(enabled) > 0 {
		val, err := strconv.Atoi(enabled)
		if err == nil {
			flag = val
		}
	}

	list := GetDocumentList()
	response := Response{Code: len(list), List: make([]Item, len(list))}

	for _, doc := range list {
		item := Item{}
		item.Path = doc.Path
		item.Remark = doc.Remark
		item.Extdata = doc.Extdata
		switch strings.ToLower(doc.Access) {
		case "public":
			item.Enabled = 3
		case "protect":
			item.Enabled = 2
		default:
			item.Enabled = 1
		}
		if path == "" || strings.HasPrefix(item.Path, path) {
			if auth > 0 {
				if auth <= item.Enabled {
					response.List[num] = item
					num++
				}
			} else {
				if flag < 0 || flag == item.Enabled {
					response.List[num] = item
					num++
				}
			}
		}
	}

	response.List = response.List[:num]

	ctx.JSON(200, response)
}

func getcgidoclist(ctx *gin.Context) {
	type Item struct {
		Url     string `json:"url"`
		Title   string `json:"title"`
		Folder  string `json:"folder"`
		Remark  string `json:"remark"`
		Enabled string `json:"enabled"`
	}

	type Response struct {
		Code int    `json:"code"`
		List []Item `json:"list"`
	}

	list := GetDocumentList()
	sort.Sort(list)
	response := Response{Code: len(list), List: make([]Item, len(list))}

	for i, doc := range list {
		item := Item{}
		item.Url = fmt.Sprintf("/cgidoc?padding=0px&path=%s", doc.Path)
		item.Title = doc.Path
		item.Folder = "gotest"
		item.Remark = doc.Remark
		response.List[i] = item
	}

	ctx.JSON(200, response)
}
