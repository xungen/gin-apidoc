package apidoc

func getTemplate() string {
	return `
<!DOCTYPE HTML>
<html>
<head>
<title>接口文档</title>
<meta name='referrer' content='always'/>
<meta http-equiv='x-ua-compatible' content='ie=edge,chrome=1'/>
<meta http-equiv='content-type' content='text/html; charset=utf-8'/>
<meta name='viewport' content='width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no'/>
<style>
.CgiDocDiv{
	width: 100%;
	font-size: 13px;
	padding: <%=padding%>;
	display: inline-block;
	box-sizing: border-box;
	font-family: '宋体', 'Helvetica', 'Tahoma', 'Verdana', 'Courier New';
}
.CgiDocHeadDiv{
	width: 100%;
	padding: 4px;
	display: inline-block;
	box-sizing: border-box;
	border-bottom: 1px solid #AAA;
}
.CgiDocPathDiv{
	color: #B00;
	font-size: 2.4rem;
	letter-spacing: -1px;
	display: inline-block;
}
.CgiDocRemarkDiv{
	color: #456;
	font-size: 1.8rem;
	font-weight: bold;
	padding-left: 8px;
	letter-spacing: -1px;
	display: inline-block;
}
.CgiDocParamDiv{
	padding: 4px;
	margin-top: 24px;
	font-weight: bold;
	font-size: 1.4rem;
}
.CgiDocListTable{
	width: 100%;
	background: #FFF;
	border-radius: 4px;
	border-collapse: collapse;
}
.CgiDocListTable span{
	color: #098;
	padding-right: 4px;
}
.CgiDocListTable td{
	padding: 2px 8px;
	line-height: 18px;
}
.CgiDocListTable tr:first-child{
	background: #888;
	font-weight: bold;
	font-size: 0.95rem;
}
.CgiDocListTable tr td:nth-child(1){
	min-width: 15vw;
}
.CgiDocListTable tr td:nth-child(2){
	min-width: 5vw;
}
.CgiDocListTable tr td:nth-child(3){
	min-width: 5vw;
}
.CgiDocListTable tr td:nth-child(4){
	min-width: 25vw;
}
</style>
</head>

<body>
	<div class='CgiDocDiv'>
		<div class='CgiDocHeadDiv'>
			<div class='CgiDocPathDiv'><%=path%></div>
			<div class='CgiDocRemarkDiv'><%=remark%></div>
		</div>

		<div class='CgiDocParamDiv'>请求参数</div>
		<table class='CgiDocListTable'>
				<tr><td>字段</td><td>类型</td><td>属性</td><td>说明</td></tr>
				<%=reqdoc%>
		</table>

		<div class='CgiDocParamDiv'>应答参数</div>
		<table class='CgiDocListTable'>
				<tr><td>字段</td><td>类型</td><td>属性</td><td>说明</td></tr>
				<%=rspdoc%>
		</table>
	</div>
</body>
</html>
`
}
